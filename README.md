[![Tag](https://img.shields.io/gitlab/v/tag/japtain_cack/sshac?style=for-the-badge)](https://gitlab.com/japtain_cack/sshac/-/tags)
[![Gitlab Pipeline](https://img.shields.io/gitlab/pipeline-status/japtain_cack/sshac?branch=master&style=for-the-badge)](https://gitlab.com/japtain_cack/sshac/-/pipelines)
[![Issues](https://img.shields.io/gitlab/issues/open/japtain_cack/sshac?style=for-the-badge)](https://gitlab.com/japtain_cack/sshac/-/issues)
[![License](https://img.shields.io/badge/License-CC%20BY--ND%204.0-blue?style=for-the-badge)](https://creativecommons.org/licenses/by-nd/4.0/)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg?style=for-the-badge)](https://github.com/psf/black)

# Overview
This tool allows you to completely manage your `.ssh/config` via yaml. It uses self defined Providers
to automatically cross reference instances and gather data like IPs, name tags, etc. SSHaC will then use
this collected data to automatically and dynamically build your `.ssh/config` file. AWS is the only provider
currently supported, but it has the capability to be expanded further in the future.


![SSHaC CLI](http://publicsj.mimir-tech.org/sshgen.png)

# Installation
## Install with package repository
Install from the [package repository](${CI_PROJECT_URL}/-/packages/?search[]=sshac) using pip.
This method supports updating this package with pip or any package manager that supports pip methods
of installation.

NOTE: **You do NOT** need to use the personal access token creds. Simply omit those when supplying pip
  the url.

* Latest: `pip install -U sshac --extra-index-url https://gitlab.com/api/v4/projects/40078067/packages/pypi/simple`
* 1.0.0: `pip install -U sshac==1.0.0 --extra-index-url https://gitlab.com/api/v4/projects/40078067/packages/pypi/simple`

## Install by cloning the repo
* `git clone https://gitlab.com/japtain_cack/sshac.git`
* `cd sshac`
* `pip install .`

## Install wheel artifact directly
* Latest: `pip install -U $https://gitlab.com/japtain_cack/sshac/-/jobs/artifacts/master/raw/dist/sshac-latest-py3.whl?job=build`
* 1.0.0: `pip install -U $https://gitlab.com/japtain_cack/sshac/-/jobs/artifacts/1.0.0/raw/dist/sshac-1.0.0-py3.whl?job=build`

# Usage
## Authentication
This tool adhears to best practics and does not use keys. Instead you must authenticate your environment.

## YAML File schema
### Top level keys `[list[Dict[str,Any]]]`
* `profile` `str` should match your aws profile name. You can specify more than one AWS profile. Each profile should follow the same yaml
  structure in the example yaml provided in this repo.
* `provider` `str` This is a simple string to determine which Provider will be used to look up your instance data. If you specify the `null`
  provider in the yaml file, you must surround the string with single quotes. Example `'null'`.
  Currently, `aws` and `null` providers are available.
* `hosts` `List[Dict[str,str]]` This key contains a list of hosts and their options. See **Host level options** below.
* `static_hosts` `List[Dict[str,str]]` This is a 1:1 mapping of host entries, similar to the `hosts` section, but no Provider will be used to look up
  any data. It will be placed into your `.ssh/config` as is.
* `globals` `Dict[str,str]` These are `host` options that will be applied globally, per profile. Any options supplied directly to a host entry,
  will override the global option.
### Host level keys `[List[Dict[str,Any]]]`
* `name` `str` This is the name or partial name you wish to match against. If more than one instance is matched, and has the same name,
  then all hosts will be added and a number will be appended to the additional hosts. Currently this is simply an
  `if host in instance_name` style match, and the aws provider passes in your host string as a `Name` tag filter. This may be
  expanded in the future if there is a need.
* `name_override` `str` can be used to set a desired ssh config host name; The name you will type in your `ssh ...` command.
  It should be noted, that if `name` matches multiple instances, your name override will have a number appended for duplicate hosts.
* `hostname_override` `str` will be used instead of automatically obtaining the private IP address.
* `region` `str` For Providers that require a region, this key can be provided.
* `other options` `str` will be passed in as is. If an option is case sensitive, you must define it in your yaml exactly as you want it
  to apper in your ssh config.

## Additional Information
* You can specify any number of SSH parameters in the `hosts`,`static_hosts`, or `globals` sections and they will be pulled into
  your ssh config as is.
* If you don't specify an `.ssh/config` or an `.ssh/config.yaml` using the `-s`/`-c` flags respectively, these aformentioned files
  are the defaults.
* Using the `--clean` flag, will wipe your ssh config on each run. This is useful when managing your ssh config entirely with this
  tool. Such as running this as a cron job, or repeating task.

# YAML Configuration
See the [config.yaml](https://gitlab.com/japtain_cack/sshac/-/blob/master/config.yaml).
