#!/usr/bin/env python3
"""
SSHaC: Generate .ssh/config programatically or automatically to be
    run on a schedule.
"""

import logging
import sys
from pathlib import Path
from typing import Optional
from enum import Enum
from functools import wraps
from asyncio import run as aiorun
from pkg_resources import get_distribution

import structlog
import typer
from rich.console import Console
from rich.table import Table
from rich import print, print_json
from botocore.exceptions import ClientError, ProfileNotFound

from sshac.ssh import SSHaC

__version__ = get_distribution("sshac").version


logger = structlog.stdlib.get_logger()


class YAMLConfigNotFound(FileNotFoundError):
    """
    yaml file not found exception
    """


class Providers(str, Enum):
    """
    Provider choices
    """

    aws = "aws"
    null = "null"


def version_callback(value: bool):
    if value:
        print(f"SSHaC CLI Version: {__version__}")
        raise typer.Exit()


def run_async(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        async def coro_wrapper():
            return await func(*args, **kwargs)

        return aiorun(coro_wrapper)

    return wrapper


def main(
    config: Optional[Path] = typer.Option(
        "~/.ssh/config.yaml",
        "--config",
        "-c",
        help="Source: ssh yaml config.",
        exists=False,
    ),
    ssh_config: Optional[Path] = typer.Option(
        "~/.ssh/config",
        "--ssh_config",
        "-s",
        help="Destination: ssh config. If it doesn't exits it will be created (mode=609).",
        exists=False,
    ),
    provider: Providers = typer.Option(
        Providers.aws,
        "--provider",
        "-p",
        help="Determins the method to look up instance data",
    ),
    clean: bool = typer.Option(
        False,
        "--clean",
        help="(Destructive, overwrite mode) Discards existing ssh config. Usefull for fully managed ssh config file.",
    ),
    yes: bool = typer.Option(
        False, "--yes", "-y", help="(Destructive) Assumes yes. Will not ask yes/no."
    ),
    quiet: bool = typer.Option(
        False,
        "--quiet",
        "-q",
        help="(Destructive, Assumes yes) Suppresses all, except critical, messages and prompts.",
    ),
    force: bool = typer.Option(
        False,
        "--force",
        "-f",
        help="(Destructive) Forces overwriting of conflicting hosts in destination ssh config.",
    ),
    debug: bool = typer.Option(
        False, "--debug", "-d", help="Enable verbose debug logging. This also disables graceful handling of unknknown exceptions."
    ),
    version: Optional[bool] = typer.Option(
        None,
        "--version",
        "-v",
        help=f"({__version__}) Show this tool's version information",
        callback=version_callback,
        is_eager=True,
    ),
) -> None:
    """
    SSHaC CLI utility.

    Manage ssh config as code with a simple yaml structure.
    """

    async def cli() -> None:
        """
        SSHaC CLI utility.

        :param config: Source: YAML ssh config file
        :type config: Path
        :param ssh_config: Destination: SSH config file
        :type ssh_config: Path
        :param provider: Determins the method to look up instance data
        :type provider: str
        :param clean: (Destructive, overwrite mode) Discards existing ssh config. Usefull for fully managed ssh config file.
        :type clean: bool
        :param yes: (Destructive) Assumes yes. Will not ask yes/no.
        :type yes: bool
        :param quiet: (Destructive, Assumes yes) Suppresses all, except critical, messages and prompts.
        :type quiet: bool
        :param force: (Destructive) Forces overwriting of conflicting hosts in destination ssh config.
        :type force: bool
        :param debug: Enable verbose debug logging. This also disables graceful handling of unknknown exceptions.
        :type debug: bool
        """
        if quiet:
            verbosity = logging.CRITICAL
        else:
            verbosity = logging.INFO
        if debug:
            verbosity = logging.DEBUG

        structlog.configure(
            wrapper_class=structlog.make_filtering_bound_logger(verbosity)
        )

        try:
            sshac = SSHaC(ssh_config.expanduser(), config.expanduser())

            if clean:
                logger.debug(
                    "clean mode enabled, clearing existing ssh config entries."
                )
                sshac.clean_ssh_config()

            await sshac.generate_config(provider.lower(), force)
            report: dict = sshac.get_report()
            logger.info("SSHaC report", **report)

            if not yes and not quiet:
                print("NEW CONFIG")
                print(sshac.ssh_config.config())

                table = Table(title="Summary")
                table.add_column("Succeeded", justify="center", style="green")
                table.add_column("Failed", justify="center", style="red")
                table.add_column("Defined", justify="center", style="cyan")
                table.add_column("Total Processed", justify="center", style="yellow")
                table.add_row(
                    str(report.get("succeeded")),
                    str(report.get("failed")),
                    str(report.get("hosts_defined")),
                    str(report.get("total")),
                )
                console = Console()
                console.print(table)

                confirm: str = str(
                    input("Would you like to save these changes? (yes/no/n): ") or "no"
                ).lower()

                if confirm != "yes":
                    logger.info("Aborting without commiting changes.")
                    sys.exit(0)

            sshac.write_ssh_config()

        except YAMLConfigNotFound as error:
            logger.critical(
                "YAML config file was not found", path=config, cause=repr(error)
            )
        except ProfileNotFound as error:
            logger.critical(
                "AWS profile could not be found", path=config, cause=repr(error)
            )
        except ClientError as error:
            if error.response["Error"]["Code"] == "RequestExpired":
                logger.critical("AWS credentials have expired", cause=repr(error))
            else:
                raise error
        except Exception as error:
            logger.critical("unhandled exception encountered", cause=repr(error))
            if debug:
                raise error

    aiorun(cli())


def entrypoint():
    """
    Entrypoint
    """
    typer.run(main)


if __name__ == "__main__":
    entrypoint()
