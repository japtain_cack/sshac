"""
Parser class
"""
from typing import Generator


class Parser:
    """
    Parse and return standardized data from any provider
    """

    @staticmethod
    def parse_aws_instances(response: dict) -> Generator:
        """
        Parse AWS describe_instances response object

        :param response: boto3.client.describe_instances response object
        :type response: dict
        :yield: aws instance attributes
        :rtype: Iterator[dict]
        """
        for reservation in response.get("Reservations", {}):
            instances: list = reservation.get("Instances")

            for instance in instances:
                ip = instance.get("PrivateIpAddress")
                instance_id = instance.get("InstanceId")
                tags = instance.get("Tags")
                instance_name = None

                for tag in tags:
                    key = tag.get("Key")
                    value = tag.get("Value")
                    if key.lower() == "name":
                        instance_name = value

                yield {"id": instance_id, "ip": ip, "name": instance_name, "tags": tags}
