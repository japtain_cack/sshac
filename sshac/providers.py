"""
Data lookup providers
"""

import structlog
from boto3.session import Session as AWSSession

from .parser import Parser
from .decorators import as_async

logger = structlog.stdlib.get_logger()


class Provider:
    """
    Data lookup provider

    :param provider_type: Choose the provider type
    :type provider_type: str
    :param profile: Provider profile name. For instance the AWS profile.
    :type profile: str
    :param region: Set the region
    :type region: str
    """

    @staticmethod
    def get(provider_type: str, **kwargs):
        """
        Get specific provider
        """
        if not isinstance(provider_type, str):
            raise TypeError("provider_type must be a string.")

        logger.debug("provider type set", provider=provider_type)

        if provider_type == "aws":
            return AWS(**kwargs)

        return NullProvider()


class AWS:
    """
    AWS data lookup provider

    :param profile: Provider profile name. For instance the AWS profile.
    :type profile: str
    :param region: Set the region
    :type region: str
    """

    def __init__(self, profile: str, region: str = "us-west-2"):
        self.profile: str = profile
        self.region: str = region
        self.session: AWSSession = self._get_session(self.profile)

    def _get_session(self, profile: str = str(), region: str = str()) -> AWSSession:
        """
        Get AWS Session object

        :param profile: aws cli profile to use
        :type profile: str
        """
        profile = profile or self.profile
        region = region or self.region

        if not isinstance(profile, str):
            raise TypeError("Profile must be a valid string")
        if not isinstance(region, str):
            raise TypeError("Region must be a valid string")

        logger.debug("aws session initialized", profile=profile, region=region)
        return AWSSession(profile_name=profile, region_name=region)

    def update(self, profile: str = str(), region: str = str()) -> None:
        """
        Update AWS session. Set session.profile and session.region
        before calling this method.
        """
        profile = profile or self.profile
        region = region or self.region
        self.session = self._get_session(self.profile, self.region)

    @as_async
    def _get_instances(self, host: str, region: str = str()) -> dict:
        """
        Generate aws instances

        :param host: host to filter for
        :type host: str
        :param region: aws region
        :type region: str
        :return: aws instance data
        :rtype: dict
        """
        region = region or self.region

        if not isinstance(host, str):
            raise TypeError("host must be a valid string")
        if not isinstance(region, str):
            raise TypeError("Region must be a valid string")

        client: AWSSession = self.session.client("ec2", region_name=region)

        return client.describe_instances(
            Filters=[
                {
                    "Name": "tag:Name",
                    "Values": [
                        f"{host}*",
                    ],
                },
                {"Name": "instance-state-name", "Values": ["running", "pending"]},
            ]
        )

    async def get_hosts(self, name: str, region: str = str()):
        region = region or self.region

        if not isinstance(name, str):
            raise TypeError("name must be a valid string")

        response: dict = await self._get_instances(
            name,
            region,
        )

        return Parser.parse_aws_instances(response)


class DNS:
    """
    DNS data lookup provider
    """

    def get_hosts(self, *args, **kwargs):
        return {}


class NullProvider:
    """
    Provider to be used when no provider is defined.
    """

    async def get_hosts(self, *args, **kwargs):
        return {}
