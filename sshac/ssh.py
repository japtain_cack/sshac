"""
SSHaC: Manage SSH config as code
"""

from pathlib import Path
from typing import List, Optional, Mapping, Any
import getpass

import structlog
import yaml
from sshconf import empty_ssh_config_file, read_ssh_config

from .providers import Provider

logger = structlog.stdlib.get_logger()


class YAMLConfigNotFound(FileNotFoundError):
    """
    yaml file not found exception
    """


class SSHaC:
    """
    SSHaC Class
    """

    def __init__(
        self,
        ssh_config_path: Path = Path("~/.ssh/config"),
        ssh_config_yaml_path: Path = Path("~/.ssh/config.yaml"),
    ) -> None:
        """
        Generate ssh config from yaml input

        :param ssh_config_path: path to .ssh/config
        :type ssh_config_path: Path
        :param ssh_config_yaml_path: path to .ssh/config.yaml
        :type ssh_config_yaml_path: Path
        """
        self.report: dict = {
            "succeeded": 0,
            "failed": 0,
            "hosts_defined": 0,
            "total": 0,
            "instances": {
                "retrieved": [],
                "unretrievable": [],
            },
        }
        self.ssh_config_path: Path = ssh_config_path
        self.ssh_config_yaml_path: Path = ssh_config_yaml_path
        self.ssh_config = self._get_ssh_config()
        self.ssh_config_yaml: List[dict] = self._get_yaml_config()

    def get_report(self) -> dict:
        """
        Get status report

        :returns: dict -- metrics
        """
        self.report["succeeded"] = len(self.report["instances"]["retrieved"])
        self.report["failed"] = len(self.report["instances"]["unretrievable"])
        self.report["total"] = self.report.get("succeeded", 0) + self.report.get(
            "failed", 0
        )
        return self.report

    def _get_ssh_config(self) -> dict:
        """
        Get ssh config as object

        :return: ssh config as an object
        :rtype: dict
        """
        path: Path = self.ssh_config_path
        if not path.exists():
            path.touch(mode=0o600)
            logger.info(
                "Provided ssh config did not exist, so it was created",
                path=path,
                mode=600,
            )

        ssh_config: dict = read_ssh_config(path)
        logger.debug("SSH config loaded successfully", path=self.ssh_config_path)
        return ssh_config

    def _get_yaml_config(self) -> List[dict]:
        """
        Get ssh yaml config

        :return: SSH yaml config
        :rtype: dict
        """
        path = self.ssh_config_yaml_path
        if not path.exists():
            raise YAMLConfigNotFound(f"File not found: {path}")

        with open(path, encoding="utf-8") as fh:
            ssh_config: List[dict] = yaml.safe_load(fh)
            logger.debug("SSH yaml config imported successfully", path=path)

            if not isinstance(ssh_config, list):
                raise TypeError("SSH config yaml is malformed")

            for profile in ssh_config:
                self.report["hosts_defined"] += len(
                    profile.get("static_hosts", [])
                ) + len(profile.get("hosts", []))
            return ssh_config

    def clean_ssh_config(self) -> None:
        """
        Clean the .ssh/config file. Used for fully managing the ssh config automatically.
        """
        self.ssh_config = empty_ssh_config_file()

    def write_ssh_config(self) -> None:
        """
        Write the ssh config
        """

        self.ssh_config.write(self.ssh_config_path)
        logger.debug("ssh config saved.", path=self.ssh_config_path)

    def _add_ssh_host(
        self,
        name: str,
        force: bool = False,
        **kwargs,
    ) -> None:
        """
        Create ssh host objects

        :param name: SSH entry name, different from HostName parameter.
            This is what the entry within the ssh config will be called.
        :type name: str
        :param force: Force host updates
        :type force: bool
        """

        kwargs.pop("region", None)
        kwargs.pop("name_override", None)
        kwargs.pop("hostname_override", None)
        kwargs.pop("name", None)

        host = self.ssh_config.host(name)

        if not host:
            self.ssh_config.add(
                name,
                **kwargs,
            )
            logger.debug("host will be added on save", name=name, **kwargs)
        elif host and force:
            self.ssh_config.set(
                name,
                **kwargs,
            )
            logger.debug(
                "host exists and force=True, host will updated on save",
                name=name,
                **kwargs,
            )
        else:
            logger.warn(
                "skipping host because it already exists, and force=False, no changes will be performed",
                name=name,
                **kwargs,
            )

    def _generate_static_hosts(self, hosts: List[dict], force: bool = False) -> None:
        for host in hosts:
            constructed_host: dict = {**host}
            self.report["instances"]["retrieved"].append(constructed_host.get("name"))
            self._add_ssh_host(**constructed_host, force=force)

    async def _generate_hosts(
        self,
        provider: Provider,
        profile: str,
        hosts: List[dict],
        _globals: Optional[dict] = None,
        force: bool = False,
    ):
        for host in hosts:
            history: list = []
            instances: dict = {}
            constructed_host: dict = {**_globals, **host}
            instances = await provider.get_hosts(
                name=constructed_host.get("name"), region=constructed_host.get("region")
            )

            for instance in instances:
                if host.get("name") in instance.get("name"):
                    name: str = constructed_host.get(
                        "name_override", instance.get("name").replace(" ", "_")
                    )
                    constructed_host["HostName"] = constructed_host.get(
                        "hostname_override", instance.get("ip")
                    )
                    constructed_host["User"] = constructed_host.get(
                        "User", getpass.getuser()
                    )
                    constructed_host["#InstanceName"] = instance.get("name")
                    constructed_host["#InstanceID"] = instance.get("id")
                    constructed_host["#InstanceIP"] = instance.get("ip")
                    constructed_host["#InstanceRegion"] = constructed_host.get("region")
                    constructed_host["#Profile"] = profile

                    if name in history:
                        count: int = history.count(name) + 1
                        constructed_host["name"] = f"{name}_{count}"
                    else:
                        constructed_host["name"] = name

                    history.append(name)
                    self.report["instances"]["retrieved"].append(
                        constructed_host.get("name")
                    )
                    self._add_ssh_host(**constructed_host, force=force)

    async def generate_config(self, lookup: str = "aws", force: bool = False) -> None:
        """
        Generate the new SSHaC host data

        :param lookup: perform cloud based lookup of host data
        :type lookup: str
        """
        if (
            not isinstance(self.ssh_config_yaml, list)
            or not len(self.ssh_config_yaml) > 0
        ):
            raise TypeError(
                f"YAML appears to be empty or malformed: {self.ssh_config_yaml_path}"
            )

        for profile in self.ssh_config_yaml:
            lookup = profile.get("provider", lookup)
            provider = Provider.get(lookup, profile=profile.get("profile"))

            self._generate_static_hosts(profile.get("static_hosts", []), force)

            await self._generate_hosts(
                provider,
                profile.get("profile", str()),
                profile.get("hosts", []),
                profile.get("globals", []),
                force,
            )
            # try:
            # except TypeError as error:
            #    if '__aiter__ method' in repr(error):
            #        self.report['instances']['unretrievable'].append(constructed_host.get('name'))
            #        logger.debug('no provider/data provided for lookup', host=constructed_host.get('name'), lookup=lookup)
